# Fraud Detection

Fraud is one of the most significant issues the finance sector faces. It is incredibly costly. According to one study, it is estimated that the typical organization loses 5% of its annual revenue to fraud each year. When applied to the 2017 estimated Gross World Product of $79.6 trillion, this translates to potential global losses of up to $4 trillion.

Fraud detection is a task inherently suitable for machine learning, as machine learning-based models can scan through huge transactional datasets, detect unusual activity, and identify all cases that might be prone to fraud. Also, the computations of these models are faster compared to traditional rule-based approaches. By collecting data from various sources and then mapping them to trigger points, machine learning solutions are able to discover the rate of defaulting or fraud propensity for each potential customer and transaction, providing key alerts and insights for the financial institutions.

In this case study, we will use various classification-based models to detect whether a transaction is a normal payment or a fraud.

The focuses of this case study are :

- Handling unbalanced data by downsampling/upsampling the data.

- Selecting the right evaluation metric, given that one of the main goals is to reduce false negatives (cases in which fraudulent transactiopns incorrectly go unnoticed).
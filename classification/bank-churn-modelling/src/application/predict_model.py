from os import path
import sys

import pickle

import src.settings.settings as stg
import src.settings.user_settings as ustg

from src.infrastructure.raw_bank_data import RawBankData
from src.domain.train_model import train_model

# Import Data Test
bank_datatest = RawBankData(
    customers_filename=ustg.CUSTOMERS_TEST_FILEPATH, customers_sep=ustg.CUSTOMERS_TEST_SEPARATOR,
    indicators_filename=ustg.INDICATORS_TEST_FILEPATH, indicators_sep=ustg.INDICATORS_TEST_SEPARATOR)
X_test = bank_datatest.data

# Import model
try:
    with open(stg.MODEL_FILENAME, 'rb') as handle:
        data_model_dict = pickle.load(handle)
    model = data_model_dict['model']
except FileNotFoundError as error:
    print("No model was found. Model will be trained first.")
    data_model_dict = train_model()
    model = data_model_dict['model']

# Predict
print('Starting Model Prediction')
print('-------------------------')
y_proba = model.predict_proba(X_test)[:, 1]
threshold = 0.32800000000000024
y_pred = (y_proba >= threshold).astype(int)

# Export model with predictions
data_model_dict['X_test'] = X_test
data_model_dict['y_pred'] = y_pred

with open(stg.MODEL_FILENAME, 'wb') as handle:
    pickle.dump(data_model_dict, handle)
print("Model is saved in:")
print(f"\t{stg.MODEL_FILENAME}")

# Saving predictions in csv
X_test[stg.CHURN_PREDICTION] = y_pred
X_test[stg.CHURN_PREDICTION] = X_test[stg.CHURN_PREDICTION].map(stg.MAPPING[stg.CHURN_PREDICTION])

X_test.to_csv(stg.PREDICTIONS_FILENAME, index=False)
print("Predictions are saved in:")
print(f"\t{stg.PREDICTIONS_FILENAME}")

# Saving data without predictions
if bank_datatest.dataframe_with_nan.empty == False:
    bank_datatest.dataframe_with_nan.to_csv(stg.NO_PREDICTION_FILENAME, index=False)

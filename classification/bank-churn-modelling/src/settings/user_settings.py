from os import path
import src.settings.settings as stg

# Enter filenames :
CUSTOMERS_TEST_FILENAME = 'customers_test.csv'
INDICATORS_TEST_FILENAME = 'indicators_test.csv'
LABELS_TEST_FILENAME = 'y_test.csv'

# Enter separator for your files :
CUSTOMERS_TEST_SEPARATOR = ";"
INDICATORS_TEST_SEPARATOR = ";"

# As an indication :
CUSTOMERS_TRAIN_FILENAME = stg.CUSTOMERS_FILENAME
INDICATORS_TRAIN_FILENAME = stg.INDICATORS_FILENAME


CUSTOMERS_TEST_FILEPATH = path.abspath(path.join(stg.TEST_DATA_DIR, CUSTOMERS_TEST_FILENAME))
INDICATORS_TEST_FILEPATH = path.abspath(path.join(stg.TEST_DATA_DIR, INDICATORS_TEST_FILENAME))

if __name__ == "__main__":
    print("Train data should be stored in:")
    print(CUSTOMERS_TRAIN_FILENAME)
    print(INDICATORS_TRAIN_FILENAME)
    print("\nTest data should be stored in:")
    print(path.abspath(path.join(stg.TEST_DATA_DIR, CUSTOMERS_TEST_FILENAME)))
    print(path.abspath(path.join(stg.TEST_DATA_DIR, INDICATORS_TEST_FILENAME)))


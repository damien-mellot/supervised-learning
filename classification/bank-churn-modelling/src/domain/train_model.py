from os import path
import pickle

import pandas as pd

from sklearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestClassifier

import src.settings.settings as stg

from src.infrastructure.raw_bank_data import RawBankData
from src.infrastructure.technical_cleaning import CategoricalTypeTransformer, BooleanEncoder, DateTransformer

from src.domain.domain_cleaning import FrequencyEncoder, AberrantAgeImputer, AberrantNbProduitsImputer,\
                                       CreditScoreImputer, SalaryImputer, BalanceImputer

from src.domain.feature_engineering import SeniorityCreator, FeatureDropper, CreditScoreAgeRatioCreator,\
                                           CreditScoreByNProductsCreator

def train_model():
    """Function to train the selected model and save it in a pickle file.
    """
    # Import Data
    bank_data = RawBankData()
    X = bank_data.features
    y = bank_data.target

    # Pipeline building
    prepro = Pipeline(steps=[
        ("DateTransformer", DateTransformer(**stg.PARAMS_DATE_TRANSFORMER)),
        ("CategoricalTypeTransformer", CategoricalTypeTransformer(**stg.PARAMS_CATEGORICAL_TYPE_TRANSFORMER)),
        ("BooleanEncoder", BooleanEncoder(**stg.PARAMS_BOOLEAN_ENCODER)),
        ("FrequencyEncoder", FrequencyEncoder(**stg.PARAMS_FREQUENCY_ENCODER)),
        ("AberrantAgeImputer", AberrantAgeImputer()),
        ("AberrantNbProduitsImputer", AberrantNbProduitsImputer()),
        ("CreditScoreImputer", CreditScoreImputer(0)),
        ("SalaryImputer", SalaryImputer(**stg.PARAMS_SALARY_IMPUTER)),
        ("BalanceImputer", BalanceImputer()),
        ("SeniorityCreator", SeniorityCreator(**stg.PARAMS_SENIORITY_CREATOR)),
        ("CreditScoreAgeRatioCreator", CreditScoreAgeRatioCreator(stg.RATIO_SCORE_CREDIT_AGE)),
        ("CreditScoreByNProductsCreator", CreditScoreByNProductsCreator(stg.RATIO_SCORE_CREDIT_NB_PRODUITS)),
        ("FeatureDropper", FeatureDropper(**stg.PARAMS_FEATURE_DROPPER))
    ])

    model = Pipeline(steps=[
        ('prepro', prepro),
        ('classifier', RandomForestClassifier(max_depth=8,
                                              n_estimators=932,
                                              min_samples_split=6,
                                              min_samples_leaf=1,
                                              class_weight='balanced_subsample',
                                              criterion='entropy',
                                              random_state=40))
    ])

    # Training model
    print('Starting Model Training')
    print('-----------------------')
    model.fit(X, y)
    print('Model succesfully trained')
    print('*************************')


    # Saving model
    data_model_dict = {
        'X': X,
        'y': y,
        'model': model
    }

    with open(stg.MODEL_FILENAME, 'wb') as handle:
        pickle.dump(data_model_dict, handle)

    return data_model_dict

def evaluate_model():
    """Evaluate selected model
    """
    from sklearn.model_selection import train_test_split
    from sklearn.metrics import accuracy_score, recall_score, fbeta_score, precision_score
    from sklearn.metrics import classification_report, confusion_matrix

    # Import Data
    bank_data = RawBankData()
    X = bank_data.features
    y = bank_data.target

    # Train Val split for evaluation
    X_train, X_val, y_train, y_val = train_test_split(X, y, stratify=y, test_size=0.2, random_state=26)

    # Pipeline building
    prepro = Pipeline(steps=[
        ("DateTransformer", DateTransformer(**stg.PARAMS_DATE_TRANSFORMER)),
        ("CategoricalTypeTransformer", CategoricalTypeTransformer(**stg.PARAMS_CATEGORICAL_TYPE_TRANSFORMER)),
        ("BooleanEncoder", BooleanEncoder(**stg.PARAMS_BOOLEAN_ENCODER)),
        ("FrequencyEncoder", FrequencyEncoder(**stg.PARAMS_FREQUENCY_ENCODER)),
        ("AberrantAgeImputer", AberrantAgeImputer()),
        ("AberrantNbProduitsImputer", AberrantNbProduitsImputer()),
        ("CreditScoreImputer", CreditScoreImputer(0)),
        ("SalaryImputer", SalaryImputer(**stg.PARAMS_SALARY_IMPUTER)),
        ("BalanceImputer", BalanceImputer()),
        ("SeniorityCreator", SeniorityCreator(**stg.PARAMS_SENIORITY_CREATOR)),
        ("CreditScoreAgeRatioCreator", CreditScoreAgeRatioCreator(stg.RATIO_SCORE_CREDIT_AGE)),
        ("CreditScoreByNProductsCreator", CreditScoreByNProductsCreator(stg.RATIO_SCORE_CREDIT_NB_PRODUITS)),
        ("FeatureDropper", FeatureDropper(**stg.PARAMS_FEATURE_DROPPER))
    ])

    model = Pipeline(steps=[
        ('prepro', prepro),
        ('classifier', RandomForestClassifier(max_depth=8,
                                              n_estimators=932,
                                              min_samples_split=6,
                                              min_samples_leaf=1,
                                              class_weight='balanced_subsample',
                                              criterion='entropy',
                                              random_state=40))
    ])

    # Training model
    model.fit(X_train, y_train)

    # Predict
    y_proba = model.predict_proba(X_val)[:,1]
    threshold = 0.32800000000000024
    y_pred = (y_proba >= threshold).astype(int)

    # Results
    print("======VALIDATION======")
    print("model fbeta: %.3f" % fbeta_score(y_val, y_pred, beta=2))
    print("model recall: %.3f" % recall_score(y_val, y_pred))
    print("model accuracy: %.3f" % accuracy_score(y_val, y_pred))
    print("model precision: %.3f" % precision_score(y_val, y_pred))

    print("Confusion Matrix")
    print(confusion_matrix(y_val, y_pred))

    print("Classification Report")
    print(classification_report(y_val, y_pred))

    return

import os
import sys
import datetime
import pandas as pd

from sklearn.base import BaseEstimator, TransformerMixin

import src.settings.settings as stg

class FrequencyEncoder(BaseEstimator, TransformerMixin):
    """Transform columns with frequency encoding.

    Attributes
    ----------
    columns: list
    frequence_dict: dict

    Methods
    -------
    fit: FrequencyEncoder
        Compute frequency values.
    transform: pandas.DataFrame
        Transform categorical valued column to numerical values.
    """

    def __init__(self, columns):
        """Initialize class with dictionnary whose keys are column names to transform.
        
        Arguments
        ---------
        columns: list
        """
        self.columns = columns
        self.frequency_dict = {key:None for key in columns}

    def fit(self, X, y=None):
        """Compute frequency of each value for columns in X.

        Arguments
        ---------
        X: pandas.DataFrame
        
        Returns
        -------
        self: FrequencyEncoder
        """
        X_ = X.copy()
        for col in self.columns:
            self.frequency_dict[col] = (X_.groupby(col).size()) / len(X_)
        return self

    def transform(self, X, y=None):
        """Transform values to numerical values based on their frequency in X.

        Arguments
        ---------
        X: pandas.DataFrame
        
        Returns
        -------
        X_: pandas.DataFrame
        """
        X_ = X.copy()
        for col in self.columns:
            X_[col] = X_[col].apply(lambda x: self.frequency_dict[col][x])
        return X_

class CustomOneHotEncoder(BaseEstimator, TransformerMixin):
    """Tranform columns using one hot encoding.
    
    Attributes
    ----------
    features_to_encode: list

    Methods
    -------
    fit: OneHotEncoder
    transform: pandas.DataFrame
        Perform one hot encoding to columns.

    """

    def __init__(self, columns):
        """Initialize class.
        
        Arguments
        ---------
        columns: list
        """
        self.columns = columns
        
    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """One hot encode features.
        
        Arguments
        ---------
        X: pandas.DataFrame

        Returns
        -------
        X_: pandas.DataFrame
        """
        X_ = X.copy()
        for column in self.columns:
            X_ = pd.get_dummies(data=X_, columns=[column], prefix=column, dtype='int64')
        return X_

class AberrantAgeImputer(BaseEstimator, TransformerMixin):
    """Transform ages over 100.

    Attributes
    ----------
    no attribute needed

    Methods
    -------
    fit: AberrantAgeImputer
    transform: pandas.DataFrame
    """

    def __init__(self):
        """Initialize class."""
        pass

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """Transform age column.
        
        Arguments
        ---------
        X: pandas.DataFrame
        
        Returns
        -------
        X_: pandas.DataFrame
        """
        X_ = X.copy()
        X_[stg.AGE] = X_[stg.AGE].apply(lambda x: x % 100)
        return X_

class AberrantNbProduitsImputer(BaseEstimator, TransformerMixin):
    """Transform NB_PRODUITS over 10.

    Attributes
    ----------
    no attribute needed

    Methods
    -------
    fit: AberrantNbProduitsImputer
    transform: pandas.DataFrame
    """

    def __init__(self):
        """Initialize class."""
        pass

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """Transform NB_PRODUITS column.
        
        Arguments
        ---------
        X: pandas.DataFrame
        
        Returns
        -------
        X_: pandas.DataFrame
        """
        X_ = X.copy()
        X_[stg.NB_PRODUITS] = X_[stg.NB_PRODUITS].apply(lambda x: x % 14)
        return X_

class CreditScoreImputer(BaseEstimator, TransformerMixin):
    """Transform nan in a column.
    
    Arguments
    ---------
    value: float
    
    Methods
    -------
    fit: CreditScoreImputer
    transform: pandas.DataFrame
        Impute nan in SCORE_CREDIT column with fixed value.
    """

    def __init__(self, value):
        """Initialize class.
        
        Arguments
        ---------
        value: float
        """
        self.value = value

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """Impute nan in SCORE_CREDIT column with fixed value.
        
        Arguments
        ---------
        X: pandas.DataFrame
        
        Returns
        -------
        X_: pandas.DataFrame
        """
        X_ = X.copy()
        X_[stg.SCORE_CREDIT] = X_[stg.SCORE_CREDIT].fillna(value=self.value)
        return X_

class SalaryImputer(BaseEstimator, TransformerMixin):
    """Transform nan in a column.
    
    Arguments
    ---------
    value: float

    Methods
    -------
    fit: SalaryImputer
    transform: pandas.DataFrame
        Impute nan in SALAIRE column with fixed value.
    """

    def __init__(self, value):
        """Initialize class.
        
        Arguments
        ---------
        value: float
        """
        self.value = value
        

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """Impute nan in SALAIRE column with fixed value.
        
        Arguments
        ---------
        X: pandas.DataFrame
        
        Returns
        -------
        X_: pandas.DataFrame
        """
        X_ = X.copy()
        X_[stg.SALAIRE] = X_[stg.SALAIRE].fillna(value=self.value)
        return X_

class BalanceImputer(BaseEstimator, TransformerMixin):
    """Transform nan in a column.
    
    Arguments
    ---------
    median_balance: float

    Methods
    -------
    fit: BalanceImputer
    transform: pandas.DataFrame
    """

    def __init__(self):
        """Initialize class."""
        self.median_balance = None

    def fit(self, X, y=None):
        """Compute median of BALANCE column in X.
        
        Returns
        -------
        self: BalanceImputer
        """
        X_ = X.copy()
        self.median_balance = X_[stg.BALANCE].median()
        return self

    def transform(self, X, y=None):
        """Impute nan with value of BALANCE median calculated in fit method.
        
        Arguments
        ---------
        X: pandas.DataFrame
        
        Returns
        -------
        X_: pandas.DataFrame
        """
        X_ = X.copy()
        X_[stg.BALANCE] = X_[stg.BALANCE].fillna(value=self.median_balance)
        return X_

if __name__ == '__main__':
    from sklearn.pipeline import Pipeline
    from sklearn.compose import ColumnTransformer
    from sklearn.preprocessing import OneHotEncoder

    from src.infrastructure.raw_bank_data import RawBankData
    from src.infrastructure.technical_cleaning import DateTransformer, CategoricalTypeTransformer, BooleanEncoder

    from sklearn.linear_model import LogisticRegression

    bank_data = RawBankData()
    X = bank_data.features
    y = bank_data.target

    #Transformer building
    onehotprocessor = ColumnTransformer(
        transformers=[("OneHotEncoder", OneHotEncoder(), stg.PARAMS_ONE_HOT_ENCODER['columns'])],
        remainder='passthrough')

    #Pipeline building
    pipeline1 = Pipeline(steps=[
        ("DateTransformer", DateTransformer(**stg.PARAMS_DATE_TRANSFORMER)),
        ("CategoricalTypeTransformer", CategoricalTypeTransformer(**stg.PARAMS_CATEGORICAL_TYPE_TRANSFORMER)),
        ("BooleanEncoder", BooleanEncoder(**stg.PARAMS_BOOLEAN_ENCODER)),
        ("OneHotEncoder", CustomOneHotEncoder(**stg.PARAMS_ONE_HOT_ENCODER)),
        ("AberrantAgeImputer", AberrantAgeImputer()),
        ("AberrantNbProduitsImputer", AberrantNbProduitsImputer()),
        ("CreditScoreImputer", CreditScoreImputer(**stg.PARAMS_CREDIT_SCORE_IMPUTER)),
        ("SalaryImputer", SalaryImputer(**stg.PARAMS_SALARY_IMPUTER)),
        ("BalanceImputer", BalanceImputer())
    ])

    pipeline2 = Pipeline(steps=[
        ("DateTransformer", DateTransformer(**stg.PARAMS_DATE_TRANSFORMER)),
        ("CategoricalTypeTransformer", CategoricalTypeTransformer(**stg.PARAMS_CATEGORICAL_TYPE_TRANSFORMER)),
        ("BooleanEncoder", BooleanEncoder(**stg.PARAMS_BOOLEAN_ENCODER)),
        ("AberrantAgeImputer", AberrantAgeImputer()),
        ("AberrantNbProduitsImputer", AberrantNbProduitsImputer()),
        ("CreditScoreImputer", CreditScoreImputer(**stg.PARAMS_CREDIT_SCORE_IMPUTER)),
        ("onehotprocessor", onehotprocessor)
    ])

    result1 = pipeline1.fit_transform(X,y)
    result2 = pipeline2.fit_transform(X,y)

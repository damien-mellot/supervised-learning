import os
import sys
import datetime
import pandas as pd

from sklearn.base import BaseEstimator, TransformerMixin

import src.settings.settings as stg


class SeniorityCreator(BaseEstimator, TransformerMixin):
    """Create a column of seniority.

    Attributes
    ----------
    column_name: str

    Methods
    -------
    fit: SeniorityCreator
    transform: pandas.DataFrame
        Add a seniority column.
    """

    def __init__(self, column_name):
        """Initialize class.

        Arguments
        ---------
        column_name: str
        """
        self.column_name = column_name

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """Add a column of integers being the number of days since they became customers.

        Arguments
        ---------
        X: pandas.DataFrame

        Returns
        -------
        X_: pandas.DataFrame
        """
        X_ = X.copy()
        X_[self.column_name] = X_[stg.DATE_ENTREE].apply(lambda x: (stg.EXTRACT_DATE - x).days)
        return X_

class AgeClassCreator(BaseEstimator, TransformerMixin):
    """Create a column of age classes.
    
    Attributes
    ----------
    column_name: str
    
    Methods
    -------
    fit: AgeClassCreator
    transform: pandas.DataFrame
        Create a column of age classes.
    """

    def __init__(self, column_name):
        """Initialize class.
        
        Arguments
        ---------
        column_name: str
        """
        self.column_name = column_name

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """Create column of age classes.
        
        Arguments
        ---------
        X: pandas.DataFrame
        
        Returns
        -------
        X_: pandas.DataFrame
        """
        X_ = X.copy()
        X_[self.column_name] = pd.cut(x=X_[stg.AGE], bins=stg.CATEGORIES_AGE_DECOUPAGE, labels=stg.NOMS_CATEGORIES_AGE)
        return X_

class CreditScoreAgeRatioCreator(BaseEstimator, TransformerMixin):
    """Create a column with credit score over age ratio.
    
    Attributes
    ----------
    column_name: str
    
    Methods
    -------
    fit: CreditScoreAgeRatioCreator
    transform: pandas.DataFrame
        Create column of credit score by age ratio.
    """

    def __init__(self, column_name):
        """Initialize class.
        
        Arguments
        ---------
        column_name: str
        """
        self.column_name = column_name

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """Create a column with credit score by age ratio.
        
        Arguments
        ---------
        X: pandas.DataFrame
        
        Returns
        -------
        X_: pandas.DataFrame
        """
        X_ = X.copy()
        X_[self.column_name] = round(X_[stg.SCORE_CREDIT]/X_[stg.AGE], 2)
        return X_

class BalanceWageRatioCreator(BaseEstimator, TransformerMixin):
    """Create a column with balance over wage ratio.
    
    Attributes
    ----------
    column_name: str
    
    Methods
    -------
    fit: BalanceWageRatioCreator
    transform: pandas.DataFrame
        Create column of balance by wage ratio.
    """

    def __init__(self, column_name):
        """Initialize class.
        
        Arguments
        ---------
        column_name: str
        """
        self.column_name = column_name

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """Create a column with balance by wage ratio.
        Affect to outliers the max of the rest of the distribution.

        Arguments
        ---------
        X: pandas.DataFrame
        
        Returns
        -------
        X_: pandas.DataFrame
        """
        X_ = X.copy()
        X_[self.column_name] = X_[stg.BALANCE]/X_[stg.SALAIRE]
        
        ratio_max = X_.loc[X_[self.column_name]<1000, self.column_name].max()
        X_.loc[X_[self.column_name]>1000, self.column_name] = ratio_max

        return X_

class CreditScoreByNProductsCreator(BaseEstimator, TransformerMixin):
    """Create a column with credit score over number of products ratio.
    
    Attributes
    ----------
    column_name: str
    
    Methods
    -------
    fit: CreditScoreByNProductsCreator
    transform: pandas.DataFrame
        Create column of credit score by number of products ratio.
    """

    def __init__(self, column_name):
        """Initialize class.
        
        Arguments
        ---------
        column_name: str
        """
        self.column_name = column_name

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """Create a column with credit score by number of products ratio.
        
        Arguments
        ---------
        X: pandas.DataFrame
        
        Returns
        -------
        X_: pandas.DataFrame
        """
        X_ = X.copy()
        X_[self.column_name] = round(X_[stg.SCORE_CREDIT]/X_[stg.NB_PRODUITS], 2)
        return X_

class FeatureDropper(BaseEstimator, TransformerMixin):
    """Drop columns to a pandas.DataFrame.

    Attributes
    ----------
    columns: list

    Methods
    -------
    fit: ColumnDropper
    transform: pandas.DataFrame
        Drop columns.
    """

    def __init__(self, columns):
        """Initialize class.

        Arguments
        ---------
        columns: list
        """
        self.columns = columns

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """Drop columns.

        Arguments
        ---------
        X: pandas.DataFrame

        Returns
        -------
        X_: pandas.DataFrame
        """
        X_ = X.copy()
        X_ = X_.drop(columns=self.columns)
        return X_

if __name__ == '__main__':
    from sklearn.pipeline import Pipeline
    from sklearn.compose import ColumnTransformer

    from src.infrastructure.raw_bank_data import RawBankData
    from src.infrastructure.technical_cleaning import BooleanEncoder, CategoricalTypeTransformer, DateTransformer
    from src.domain.domain_cleaning import FrequencyEncoder, CustomOneHotEncoder, AberrantAgeImputer, AberrantNbProduitsImputer, CreditScoreImputer

    bank_dataset = RawBankData()
    X = bank_dataset.features
    y = bank_dataset.target

    #Pipeline building
    pipeline = Pipeline(steps=[
        ("DateTransformer", DateTransformer(**stg.PARAMS_DATE_TRANSFORMER)),
        ("CategoricalTypeTransformer", CategoricalTypeTransformer(**stg.PARAMS_CATEGORICAL_TYPE_TRANSFORMER)),
        ("BooleanEncoder", BooleanEncoder(**stg.PARAMS_BOOLEAN_ENCODER)),
        ("OneHotEncoder", CustomOneHotEncoder(**stg.PARAMS_ONE_HOT_ENCODER)),
        ("AberrantAgeImputer", AberrantAgeImputer()),
        ("AberrantNbProduitsImputer", AberrantNbProduitsImputer()),
        ("CreditScoreImputer", CreditScoreImputer(**stg.PARAMS_CREDIT_SCORE_IMPUTER)),
        ("SeniorityCreator", SeniorityCreator(**stg.PARAMS_SENIORITY_CREATOR)),
        ("CreditScoreAgeRatioCreator", CreditScoreAgeRatioCreator(stg.RATIO_SCORE_CREDIT_AGE)),
        ("BalanceWageRatioCreator", BalanceWageRatioCreator(stg.RATIO_BALANCE_SALAIRE)),
        ("CreditScoreByNProductsCreator", CreditScoreByNProductsCreator(stg.RATIO_SCORE_CREDIT_NB_PRODUITS)),
        ("FeatureDropper", FeatureDropper(**stg.PARAMS_FEATURE_DROPPER))
    ])

    result = pipeline.fit_transform(X, y)

    print(result)

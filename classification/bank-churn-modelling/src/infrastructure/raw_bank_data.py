import os
import sys
import random
import pandas as pd

import src.settings.settings as stg


class RawBankData:
    """Raw Bank data files and associated pandas DataFrames.

    Arguments
    ----------
    customers_filename: str
    customers_sep: str
    indicators_filename: str
    indicators_sep: str
    remove_nan: bool

    Attributes
    ----------
    customers: pandas.DataFrame
    indicators: pandas.DataFrame
    index_removed: Index
    dataframe_with_nan: pandas.DataFrame
    remove_nan: bool

    Properties
    ----------
    data: pandas.DataFrame
        The property data returns all dataframe.
    features: pandas.DataFrame
        The property features returns only features dataframe.
    target: pandas.DataFrame
        The property target return only target column and encode values to {0, 1}.
    """

    def __init__(self, customers_filename=stg.CUSTOMERS_FILENAME, customers_sep=stg.CUSTOMERS_SEP,\
                 indicators_filename=stg.INDICATORS_FILENAME, indicators_sep=stg.INDICATORS_SEP, remove_nan=True):
        """Class initialization.
        
        Arguments
        ---------
        customers_filename: str
        customers_sep: str
        indicators_filename: str
        indicators_sep: str
        remove_nan: boolean
        """
        self.customers = self._import_customers_data(customers_filename, customers_sep)
        self.indicators = self._import_indicators_data(indicators_filename, indicators_sep)
        self.remove_nan = remove_nan
        self.index_removed = None
        self.dataframe_with_nan = None

    @property
    def data(self):
        """Return full dataframe from files, with target if target exists in indicators file.
        Encode values of target in {0,1}.

        Returns
        -------
        data: pandas.DataFrame
        """
        joined_df = self._merge_datasets().copy()

        if self.remove_nan:
            joined_df = self._remove_nan(joined_df)

        if stg.CHURN in joined_df.columns:
            joined_df[stg.CHURN] = joined_df[stg.CHURN].map(stg.MAPPING[stg.CHURN])
            data = joined_df
        else:
            data = joined_df
        return data
    
    @property
    def features(self):
        """Return features dataframe from files with no target.
        
        Returns
        -------
        features: pandas.DataFrame
        """
        joined_df = self.data.copy()
        if stg.CHURN in joined_df.columns:
            features = joined_df.drop(columns=[stg.CHURN])
            return features
        else:
            features = joined_df
            return features

    @property
    def target(self):
        """Return target column from file (if exists in file).
        
        Returns
        -------
        target: pandas.core.series.Series
        """
        joined_df = self.data.copy()
        if stg.CHURN in joined_df.columns:
            target = joined_df[stg.CHURN]
            return target
        else:
            return None

    def _merge_datasets(self):
        """Merge customers and indicators data into a single dataframe.
        
        Returns
        -------
        joined_df: pandas.DataFrame
        """
        joined_df = pd.merge(self.customers, self.indicators, how='outer', on=stg.CUSTOMERS_ID_CLIENT)
        return joined_df

    def _remove_nan(self, X):
        """Remove missing values from DataFrame.
        
        Arguments
        ---------
        X: pandas.DataFrame

        Returns
        -------
        X_: pandas.DataFrame
        """
        X_ = X.copy()
        
        X_filtered = X_.filter(items=stg.MANDATORY_FIELDS)
        self.index_removed = X_filtered.index[X_filtered.isnull().any(axis=1)]
        
        self.dataframe_with_nan = X_.iloc[self.index_removed]
        
        X_ = X_.dropna(subset=stg.MANDATORY_FIELDS)
        return X_

    @staticmethod
    def _import_customers_data(filename, separator):
        """Import customers data from file.

        Returns
        -------
        customers: pandas.DataFrame
        """
        _, extension = os.path.splitext(filename)
        if extension == '.csv':
            try :
                customers = pd.read_csv(filename, sep=separator, header=0, usecols=stg.CUSTOMERS_COLUMNS,
                                        dtype={stg.CUSTOMERS_ID_CLIENT: str})
            except FileNotFoundError as error:
                print(error)
                print("!! Customers File Not Found !!\n")
        else:
            raise FileExistsError('File extension must be .csv')

        return customers

    @staticmethod
    def _import_indicators_data(filename, separator):
        """Imports indicators data from file.

        Returns
        -------
        indicators: pandas.DataFrame
        """
        _, extension = os.path.splitext(filename)
        if extension == '.csv':
            try:
                indicators = pd.read_csv(filename, sep=separator, header=0, usecols=stg.INDICATORS_COLUMNS,
                                         dtype={stg.INDICATORS_ID_CLIENT: str})
            except ValueError:
                indicators = pd.read_csv(filename, sep=separator, header=0,
                                         usecols=stg.UNLABELED_INDICATORS_COLUMNS,
                                         dtype={stg.INDICATORS_ID_CLIENT: str})
            except FileNotFoundError as error:
                print(error)
                print("!! Indicators File Not Found !!\n")
        else:
            raise FileExistsError('File extension must be .csv')
        return indicators

if __name__ == '__main__':

    bank_dataset = RawBankData()
    bank_dataframe = bank_dataset.data
    X = bank_dataset.features
    y = bank_dataset.target

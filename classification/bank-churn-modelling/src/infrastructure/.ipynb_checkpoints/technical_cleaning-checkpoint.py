import os
import sys

import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin

import src.settings.settings as stg

class DateTransformer(BaseEstimator, TransformerMixin):
    """Transform DATE to datetime format.

    Attributes
    ----------
    date_format: str

    Methods
    -------
    fit: DateTransformer
    transform: pandas.DataFrame
        Return dataframe with datetime format for DATE.
    """

    def __init__(self, date_format):
        """Initializa class.

        Arguments
        ---------
        date_format: str
        """
        self.date_format = date_format

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """Convert values in DATE_ENTREE column to pandas.datetime objects.

        Arguments
        ---------
        X: pandas.DataFrame

        Returns
        -------
        X_: pandas.DataFrame
        """
        X_ = X.copy()
        X_[stg.DATE_ENTREE] = pd.to_datetime(X_[stg.DATE_ENTREE], format=self.date_format)
        return X_

class CategoricalTypeTransformer(BaseEstimator, TransformerMixin):
    """Transform type of categorical variables into category type.

    Attributes
    ----------
    columns: list

    Methods
    -------
    fit: CategoricalTypeTransformer
    transform: pandas.DataFrame
        Return dataframe with type transformed into 'category'.
    """

    def __init__(self, columns):
        """Initialize class.

        Arguments
        ---------
        columns: list
        """
        self.columns = columns

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """Transform type columns in 'category' type.

        Arguments
        ---------
        X: pandas.DataFrame

        Returns
        -------
        X_: pandas.DataFrame
        """
        X_ = X.copy()
        for col in self.columns:
            X_[col] = X_[col].astype("category")
        return X_


class BooleanEncoder(BaseEstimator, TransformerMixin):
    """Transform categorical columns with binary encoding.

    Methods
    -------
    fit: CategoricalEncoder
    transform: pandas.DataFrame
        Change categorical columns to {0,1} columns.
    """

    def __init__(self, columns):
        """Initialize class.

        Arguments
        ---------
        columns: list
        """
        self.columns = columns

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """Changes all boolean columns to columns with zeros and ones.

        Arguments
        ---------
        X: pandas.DataFrame

        Returns
        -------
        X_: pandas.DataFrame
        """
        X_ = X.copy()
        for col in self.columns:
            X_[col] = X_[col].map(stg.MAPPING[col])
        return X_


if __name__ == '__main__':
    from sklearn.pipeline import Pipeline

    from src.infrastructure.raw_bank_data import RawBankData

    bank_data = RawBankData()
    X = bank_data.features
    y = bank_data.target

    # Pipeline building
    pipeline = Pipeline(steps=[
        ("DateTransformer", DateTransformer(**stg.PARAMS_DATE_TRANSFORMER)),
        ("CategoricalTypeTransformer", CategoricalTypeTransformer(**stg.PARAMS_CATEGORICAL_TYPE_TRANSFORMER)),
        ("BooleanEncoder", BooleanEncoder(**stg.PARAMS_BOOLEAN_ENCODER)),
    ])

    X_tr = pipeline.fit_transform(X, y)
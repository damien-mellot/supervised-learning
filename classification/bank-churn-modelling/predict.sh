# Running prediction

echo "-----------------------"
python3 src/settings/user_settings.py 
echo "-----------------------"


echo "\n"
echo "Churn prediction"
echo "-------------------------"
python3 src/application/predict_model.py && (
    echo "*********************************************************"
    echo "End of prediction"
    echo "*********************************************************"
)
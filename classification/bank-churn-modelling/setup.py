from setuptools import find_packages, setup

setup(
    name='churnjr',
    packages=find_packages(),
    version='0.1.0',
    description='Project1 - Advanced ML - Churn modelling',
    author='JeromeA&Robin',
    license='',
)

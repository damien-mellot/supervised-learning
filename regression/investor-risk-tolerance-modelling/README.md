# Investor Risk Tolerance Modeling

The goal of this case study is to build a machine learning model to predict the risk tolerance or risk aversion of an investor, and use the model in a robo-advisor dashboard.

The **risk tolerance of an investor** is one of the most important inputs to the portfolio allocation and rebalancing steps of the portfolio management process. There is a wide variety of risk profiling tools that take varied approaches to understanding the risk tolerance of an investor. Most of these approaches include qualitative judgment and involve significant manual effort. In most of the cases, the risk tolerance of an investor is decided based on a risk tolerance questionnaire.

several studies have shown that these risk tolerance questionnaires are prone to error, as investors suffer from behavorial biases and are poor judges of their own risk perception, especially during stressed markets. Also, given that these questionnaires must be manually completed by investors, they eliminate the possibility of automating the entire investment management process.

Can machine learning provide a better understanding of an investor's risk profile than a risk tolerance questionnaire can?
Can machine learning contribute to automating the entire portfolio management process by cutting the client out of the loop?
Could an algorithm be written to develop a personality profile for the client that would be a better representation of how they would deal with different market scenarios?

The goal of this case study is to answer these questions. We first build a supervised regression-based model to predict the risk tolerance of an investor. We then build a robo-advisor in Python and implement the risk tolerance prediction model in the dashboard. The overall purpose is to demonstrate the automation of manual steps in the portfolio management process with the help of machine learning. 
This can prove to be immensely useful, specifically for robo-advisors.

A *dashboard* is one of the key features of a robo-advisor as it provides access to important information and allows users to interact with their accounts free of any human dependency, making the portfolio management process highly efficient. It performs end-to-end asset allocation for an investor, embedding the machine learning-based risk tolerance model constructed in this case study.

In this case study, we will focus on:

- Feature elimination and feature importance/intuition
- Using machine learning to automate manual processes involved in portfolio management process
- Using machine learning to quantify and model the behavorial bias of investors/individuals
- Embedding machine learning models into user interfaces or dashboards using Python
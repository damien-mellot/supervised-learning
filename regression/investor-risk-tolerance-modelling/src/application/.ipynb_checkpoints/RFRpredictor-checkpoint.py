import src.settings.base as stg
from src.domain.processing import Processor

from sklearn.model_selection import KFold
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor

def fitted_RFRmodel():
    
    df = Processor().processed_data
    Y = df[stg.TRUE_RISK_TOLERANCE]
    X = df.loc[:, df.columns != stg.TRUE_RISK_TOLERANCE]
    
    validation_size = stg.validation_size
    seed = stg.seed
    num_folds = stg.num_folds
    scoring = stg.scoring
    
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=validation_size, random_state=seed)
    
    rfr = RandomForestRegressor(n_estimators=300)
    rfr_fit = rfr.fit(X_train, Y_train)
    
    return rfr_fit
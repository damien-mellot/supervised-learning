import os
from os import path
from os.path import join
import sys

import pandas as pd

import src.settings.base as stg


class DataFrameBuilder:
    
    def __init__(self, filename):
        
        """Initialize Class

        Parameters
        ----------
        filename : string
            filename of dataset (must be csv for now)
        """
        
        self.filename = filename
    
    
    @property
    def data(self):
        
        if self.filename.endswith(".xlsx"):
            df = pd.read_excel(join(stg.DATA_DIR, self.filename))
            
            return df
        
        if self.filename.endswith(".csv"):
            df = pd.read_csv(join(stg.DATA_DIR, self.filename), index_col = 0)
            
            return df
                    
        else:
            raise FileExistsError("Extension must be xlsx or csv.")

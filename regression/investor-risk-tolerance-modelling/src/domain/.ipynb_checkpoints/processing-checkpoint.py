import os
from os import path
from os.path import join
import sys

import pandas as pd
import numpy as np

import src.settings.base as stg
from src.infrastructure.datasetcreation import DataFrameBuilder
from src.domain.featurecreation import FeatureCreator
from src.domain.featureelimination import FeatureEliminator

class Processor:
    """Class to abstract pipeline creation"""

    def __init__(self):
        """class initilisation"""
    
    @property
    def processed_data(self):
        
        df = DataFrameBuilder(stg.SCF_data).data
        df = FeatureCreator().working_data(df)
        df = FeatureEliminator().final_data(df)
        df = df.reset_index(drop=True)
            
        return df
import os
from os import path
from os.path import join
import sys

import pandas as pd
import numpy as np

import src.settings.base as stg
from src.infrastructure.datasetcreation import DataFrameBuilder


class FeatureCreator:
    
    def __init__(self):
        
        """Initialize Class

        Parameters
        ----------
        df : string
            filename of dataset (must be csv for now)
        """
        
        
    def working_data(self, X):
            
        df = X.copy()
            
        # Compute the risky assets and risk-free assets for 2007 + Risk Tolerance
        df['Riskfree07'] = df['LIQ07'] + df['CDS07'] + df['SAVBND07'] + df['CASHLI07']
        df['Risky07'] = df['NMMF07'] + df['STOCKS07'] + df['BOND07'] 
        df['RT07'] = df['Risky07']/(df['Risky07'] + df['Riskfree07'])
            
        # Compute the risky assets and risk-free assets for 2009 + Risk Tolerance
        df['Riskfree09']= df['LIQ09'] + df['CDS09'] + df['SAVBND09'] + df['CASHLI09']
        df['Risky09'] = df['NMMF09'] + df['STOCKS09'] + df['BOND09']
        df['RT09'] = df['Risky09']/(df['Risky09'] + df['Riskfree09'])*\
        (stg.Average_SP500_2009/stg.Average_SP500_2007)
            
        # Compute the percentage change between 2007 and 2009
        df['PercentageChange'] = np.abs(df['RT09']/df['RT07']-1)
            
        # Drop the rows containing NA
        df = df.dropna(axis=0)
        df = df[~df.isin([np.nan, np.inf, -np.inf]).any(1)]
            
        # Picking the intelligent investors whose risk tolerance change between 2007 and 2009 was less than 10%
        df = df[df['PercentageChange'] <= .1]
            
        # Assigning the true risk tolerance as the average risk tolerance of these intelligent investors between 2007 and 2009.
        # ====> This is the predicted variable of the case.
        df['TrueRiskTolerance'] = (df['RT07'] + df['RT09'])/2
            
        # Dropping other labels which might not be needed for the prediction.
        df.drop(labels=['RT07', 'RT09'], axis=1, inplace=True)
        df.drop(labels=['PercentageChange'], axis=1, inplace=True)
            
        return df
            
            
            
            
            
            
            
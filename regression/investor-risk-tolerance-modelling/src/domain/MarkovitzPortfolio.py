import numpy as np
import pandas as pd
import cvxopt as opt
from cvxopt import blas, solvers

from src.domain.marketdata import assets_data

#Asset allocation given the Return, variance
def get_asset_allocation(riskTolerance, stock_ticker):
    #ipdb.set_trace()
    assets_selected = assets_data().loc[:, stock_ticker]
    return_vec = np.array(assets_selected.pct_change().dropna(axis=0)).T
    n = len(return_vec)
    returns = np.asmatrix(return_vec)
    mus = 1-riskTolerance
    
    # Convert to cvxopt matrices
    S = opt.matrix(np.cov(return_vec))
    pbar = opt.matrix(np.mean(return_vec, axis=1))
    
    # Create constraint matrices
    G = -opt.matrix(np.eye(n))   # negative n x n identity matrix
    h = opt.matrix(0.0, (n ,1))
    A = opt.matrix(1.0, (1, n))
    b = opt.matrix(1.0)
    
    # Calculate efficient frontier weights using quadratic programming
    portfolios = solvers.qp(mus*S, -pbar, G, h, A, b)
    w = portfolios['x'].T
    print (w)
    Alloc = pd.DataFrame(data = np.array(portfolios['x']),index = assets_selected.columns)

    # Calculate efficient frontier weights using quadratic programming
    portfolios = solvers.qp(mus*S, -pbar, G, h, A, b)
    returns_final = (np.array(assets_selected) * np.array(w))
    returns_sum = np.sum(returns_final,axis =1)
    returns_sum_pd = pd.DataFrame(returns_sum, index = assets_data().index)
    returns_sum_pd = returns_sum_pd - returns_sum_pd.iloc[0,:] + 100   
    
    return Alloc, returns_sum_pd
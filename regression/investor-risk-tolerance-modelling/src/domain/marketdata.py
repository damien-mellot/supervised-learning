import os
from os import path
from os.path import join
import sys

import pandas as pd
import numpy as np

import src.settings.base as stg
from src.infrastructure.datasetcreation import DataFrameBuilder

def investors_data():
    
    investors = DataFrameBuilder(stg.INPUT_data).data
    
    return investors


def assets_data():
    
    assets = DataFrameBuilder(stg.SP500_data).data
    
    missing_fractions = assets.isnull().mean().sort_values(ascending=False)
    
    drop_list = sorted(list(missing_fractions[missing_fractions > 0.3].index))

    assets.drop(labels=drop_list, axis=1, inplace=True)

    # Fill the missing values with the last value available in the dataset. 
    assets = assets.fillna(method='ffill')
    
    return assets


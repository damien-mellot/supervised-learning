import os
from os import path
from os.path import join
import sys

import pandas as pd
import numpy as np

import src.settings.base as stg
from src.infrastructure.datasetcreation import DataFrameBuilder
from src.domain.featurecreation import FeatureCreator

class FeatureEliminator:
    
    def __init__(self):
        
        """Initialize Class

        Parameters
        ----------
        df : string
            filename of dataset (must be csv for now)
        """
    
    
    def final_data(self, X):
        
        df = X.copy()
        
        keep_list = ['AGE07','EDCL07','MARRIED07','KIDS07','OCCAT107',
              'INCOME07','RISK07','NETWORTH07','TrueRiskTolerance']

        drop_list = [col for col in df.columns if col not in keep_list]

        df.drop(labels=drop_list, axis=1, inplace=True)
        
        return df
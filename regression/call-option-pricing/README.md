# Call Option Pricing

In computational finance and risk management, several numerical methods (e.g., finite differences, Fourier methods, and Monte Carlo simulations) are commonly used for the valuation of financial derivatives.

The **Black-Scholes formula** is probably one of the most widely cited and used models in derivative pricing. Numerous variations and extensions of this formula are used to price many kinds of financial derivatives. However, the model is based on several assumptions. It assumers a specific form of movement for the derivative price, namely a *Geometric Brownian Motion* (GBM). Is also assumes a conditional payment at maturity of the option and economic constraints, such as no-arbitrage. Several other derivative pricing models have similarly impractical model assumptions. Finance practitioners are well aware that these assumptions are violated in practice, and prices from these models are further ajusted using practitioner judgement.

Another aspect of the may traditional derivative pricing models is *model calibration*, which is typically done not by historical asset prices but by means of derivative prices(i.e. matching the market prices of heavily traded options to the derivative prices from the mathematical model). In the process of model calibration, thousands of derivative prices need to be determined in order to fit the parameters of the model, and the overall process is time consuming. Efficient numerical computation is increasingly important in risk management, especially when we deal with real-time risk management (e.g. high frequency trading). However, due to the requirement of highly efficient computation, certain high-quality asset models and methodologies are discarded during model calibration of tradtional derivative pricing models.

Machine learning can potentially be used to tackle these drawbacks related to impractical model assumptions and inefficient model calibration. Machine learning algorithms have the ability to tackle more nuances with very few theoretical assumptions and can be effectively used for derivative pricing, even in a world with frictions. With the advencement in hardware, we can train machine learning models on high performance CPUs, GPUs, and other specialized hardware to achieve a speed increase of several orders of magnitude to the traditional derivative pricing models.

Additionally, market data is plentiful, so it is possible to train a machine learning algorithm to learn the function that is collectively generating derivative prices in the market. Machne learning models can capture subtle nonlinearities in the data that are not obtainable through other statistical approaches.

The goal of this case study is to perform derivative pricing from a machine learning standpoint and use supervised regression-based model to learn the Black-Scholes option pricing model from simulated data.

The main idea here is to come up with a machine learning framework for derivative pricing. Achieving a machine learning model with high accuracy would mean that we can leverage the efficient numerical calculation of machine learning for derivative pricing with fewer underlying model assumptions.

In this case study, we will focus on:

- Developing a machine learning-based framework for derivative pricing
- Comparison of linear and nonlinear supervised regression models in the context of derivative pricing
import numpy as np
from scipy.stats import norm
import pandas as pd

from src.infrastructure.optionsfunctions import option_vol_from_surface, call_option_price
import src.settings.base as stg

class CallData:
    
    """Collect moneyness, time-to-maturity, sigmas and calls data and build DataFrame

    Attributes
    ----------
    
    Methods
    -------

    """

    def __init__(self):
        """initialise class

        Parameters
        ----------
        
    """

    @property
    def data(self):
        
        # Generate Moneyness (normally distributed random variable, times 0.25 for deviation of strike price from spot price)
        # The overall equation ensures that the moneyness is greater than zero.
        Ks = 1 + 0.25*np.random.randn(stg.N)
        
        # Generate Time to maturity (uniform random variable between 0 and 1)
        Ts = np.random.random(stg.N)
        
        # Generate Sigmas
        Sigmas = np.array([option_vol_from_surface(k, t) for k,t in zip(Ks, Ts)])
        
        # Generate Calls
        Ps = np.array([call_option_price(k,t,sig) for k,t,sig in zip(Ks, Ts, Sigmas)])
        
        # Generate DataFrame
        Y = Ps
        X = np.concatenate([Ks.reshape(-1,1), 
                            Ts.reshape(-1,1), 
                            Sigmas.reshape(-1,1)], axis=1)
    
        df = pd.DataFrame(np.concatenate([Y.reshape(-1,1), X], axis=1),
                  columns=[stg.CALL, stg.MONEYNESS, stg.TIME, stg.VOLATILITY])
    
        return df
        

# Stock Price Prediction 

One of the biggest challenges in finance is predicting stock prices. However, with the onset of recent advancements in machine learning applications, the field has been evolving to utilize nondeterministic solutions that learn what is going on in order to make more accurate predictions. Machine learning techniques naturally lend themselves to stock price prediction based on historical data. Predictions can be made for a single time point ahead or for a set of future time points.

As a high-level overview, other than the historical price of the stock itself, the features that are generally useful for stock price prediction are as follows:

*Correlated assets*
An organization depends on and interacts with many external factors, including its competitors, clients, the gobal economy, the geopolitical situation, fiscal and monetary policies, access to capital, and so on. Hence, its stock price may be correlated note only with the stock price of other companies but also with other assets such as commodities, FX, broad-based indices, or even fixed income securities.

*Technical indicators*
A lot of investors follow technical indicators. Moving average, exponential moving average, and momentum are the most popular indicators.

*Fundamental analysis*
Two primary data sources to glean features that can be used in fundamental analysis include:
 
 - *Performance reports*
 Annual and quaterly reports of companies can be used to extract or determine key metrics, such as ROE (Return on Equity) and
 P/E (Price-to-Earnings)
 
 - *News*
 News can indicate upcoming events that can potentially move the stock price in a certain direction.
 
In this case study, we will use various supervised learning-based models to predict the stock price of Microsoft using correlated assets and its own historical data. By the end of this case study, we will be familiar with a general machine learning approach to stock prediction modelling, from gathering and cleaning data to building and tuning models.

In this case study, we will focus on:

- Looking at various machine learning and time series models, ranging in complexity, that can be used to predict stock returns
- Visualization of the data using different kinds of charts(i.e., density, correlation, scatterplot, etc.)
- Using deep learning (LSTM) models for time series forecasting
- Implementation of the grid search for time series models (i.e., ARIMA model)
- Interpretation of the results and examining potential overfitting and underfitting of the data across the models.


import numpy as np
from os.path import join
import pandas as pd
import pandas_datareader as pdr

import src.settings.base as stg


class DataFrameBuilder:
    
    def __init__(self, filename):
        
        """Initialize Class

        Parameters
        ----------
        filename : string
            filename of dataset (must be csv for now)
        """
        
        self.filename = filename
    
    
    @property
    def data(self):
        
        if self.filename.endswith(".csv"):
            df = pd.read_csv(join(stg.RAW_DATA_DIR, self.filename))
            df = df.rename(columns={"Unnamed: 0": "Date"})
            df = df.set_index('Date')
                    
        else:
            raise FileExistsError("Extension must be csv.")

        return df
    
    @property
    def features(self):
        """Return features dataframe from files with no target.
        
        Returns
        -------
        features: pandas.DataFrame
        """
        dataset_df = self.data.copy()
        if stg.MSFT_PRED in dataset_df.columns:
            features = dataset_df.drop(columns=[stg.MSFT_PRED])
            return features
        else:
            features = dataset_df
            return features
    
    @property
    def target(self):
        """Return target column from file (if exists in file).
        
        Returns
        -------
        target: pandas.core.series.Series
        """
        dataset_df = self.data.copy()
        if stg.MSFT_PRED in dataset_df.columns:
            target = dataset_df[stg.MSFT_PRED]
            return target
        else:
            return None
import numpy as np
import pandas as pd
import pandas_datareader as pdr

import src.settings.base as stg


class DatasetCollecter:
    
    """Collect stock, currency exchange rate and indices data

    Attributes
    ----------
    
    Methods
    -------

    """

    def __init__(self):
        """initialise class

        Parameters
        ----------
        
    """
          
    @property
    def series_to_supervised(self, lag=1):
        
        n_vars = self.combine_datasets.shape[1]
        df = pd.DataFrame(self.combine_datasets)    
        cols, names = list(), list()
        
        for i in range(lag, 0, -1):
            cols.append(df.shift(i))
            names += [('%s(t-%d)' % (df.columns[j], i)) for j in range(n_vars)]
            
        agg = pd.concat(cols, axis=1)
        agg.columns = names
        agg = pd.DataFrame(self.combine_datasets.iloc[:,0]).join(agg)
        agg.dropna(inplace=True)
        
        
        return agg
    
    @property
    def combine_datasets(self, return_period = 5):
        
        stk_df = self._collect_stk_data().copy()
        ccy_df = self._collect_ccy_data().copy()
        idx_df = self._collect_idx_data().copy()

        Y = np.log(stk_df.loc[:, ('Adj Close', stg.MSFT)]).diff(return_period).shift(-return_period)
        Y.name = Y.name[-1]+'_pred'
        
        X1 = np.log(stk_df.loc[:, ('Adj Close', (stg.GOOGL,stg.IBM))]).diff(return_period)
        X1.columns = X1.columns.droplevel()
        
        X2 = np.log(ccy_df).diff(return_period)
        X3 = np.log(idx_df).diff(return_period)
        
        
        X4 = pd.concat([Y.diff(i) for i in [21, 63, 126,252]], axis=1).dropna()
        X4.columns = [stg.MSFT_1M, stg.MSFT_3M, stg.MSFT_6M, stg.MSFT_1Y]
        
        X = pd.concat([X1, X2, X3, X4], axis=1)
        
        dataset = pd.concat([Y, X], axis=1).dropna()

        return dataset
    
    
    def _collect_stk_data(self, stk_tickers=[stg.MSFT, stg.IBM, stg.GOOGL], start=stg.start):
        
        stk_data = pdr.DataReader(stk_tickers, stg.yahoo, start=stg.start)
        
        return stk_data
    
    
    def _collect_ccy_data(self, ccy_tickers=[stg.DEXJPUS, stg.DEXUSUK], start=stg.start):
        
        ccy_data  = pdr.DataReader(ccy_tickers, stg.fred, start=stg.start)
        
        return ccy_data

    
    def _collect_idx_data(self, idx_tickers=[stg.SP500, stg.DJIA, stg.VIXCLS], start=stg.start):
        
        idx_data = pdr.DataReader(idx_tickers, stg.fred, start=stg.start)
        
        return idx_data    
    
import numpy as np
import pandas as pd

from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_squared_error

from statsmodels.tsa.arima.model import ARIMA

import src.settings.base as stg
from src.infrastructure.datasetcreation import DataFrameBuilder
from src.domain.datatransformation import DatasetTransformer

def arima_mse(validation_size = 0.2, 
              num_folds = 10,
              scoring = 'neg_mean_squared_error',
              name = 'Autoregressive integrated moving average'):
    
    # Loading dataset and separate features from target
    dfb = DataFrameBuilder(stg.AMZN_data)
    X = dfb.features
    Y = dfb.target
    
    # Train - Test split
    train_size = int(len(X) * (1 - validation_size))
    X_train, X_test = X[0:train_size], X[train_size:len(X)]
    Y_train, Y_test = Y[0:train_size], Y[train_size:len(X)]
    
    # Train - Test split ARIMA
    X_train_ARIMA = X_train.loc[:, [stg.GOOGL, stg.IBM, stg.AAPL, 
                                    stg.DEXJPUS, stg.DEXUSEU, stg.DEXUSUK ,
                                    stg.SP500, stg.DJIA, stg.VIXCLS]]
    X_test_ARIMA = X_test.loc[:, [stg.GOOGL, stg.IBM, stg.AAPL, 
                                  stg.DEXJPUS, stg.DEXUSEU, stg.DEXUSUK , 
                                  stg.SP500, stg.DJIA, stg.VIXCLS]]
    
    tr_len = len(X_train_ARIMA)
    te_len = len(X_test_ARIMA)
    to_len = len(X)

    modelARIMA = ARIMA(endog=Y_train, exog=X_train_ARIMA, order=[1,0,0])
    model_fit = modelARIMA.fit()

    error_Training_ARIMA = mean_squared_error(Y_train, model_fit.fittedvalues)
    predicted = model_fit.predict(start = tr_len -1 ,end = to_len -1, exog = X_test_ARIMA)[1:]
    error_Test_ARIMA = mean_squared_error(Y_test,predicted)
    
    return f"{name}: Train MSE ({round(error_Training_ARIMA, 5)}) Test MSE({round(error_Test_ARIMA, 5)})"


    
    
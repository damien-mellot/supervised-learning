import numpy as np
import pandas as pd

from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import mean_squared_error

import src.settings.base as stg
from src.infrastructure.datasetcreation import DataFrameBuilder
from src.domain.datatransformation import DatasetTransformer

def mlpr_mse(validation_size = 0.2, 
        num_folds = 10,
        scoring = 'neg_mean_squared_error',
        name = 'Multi Layer Perceptron Regressor'):
    
    # Loading dataset and separate features from target
    dfb = DataFrameBuilder(stg.AMZN_data)
    X = dfb.features
    Y = dfb.target
    
    # Train - Test split
    train_size = int(len(X) * (1 - validation_size))
    X_train, X_test = X[0:train_size], X[train_size:len(X)]
    Y_train, Y_test = Y[0:train_size], Y[train_size:len(X)]
    
    ## Full training period
    mlpr = MLPRegressor()
    mlpr.fit(X_train, Y_train)
    train_result = mean_squared_error(mlpr.predict(X_train) , Y_train)
    test_result = mean_squared_error(mlpr.predict(X_test), Y_test)
    
    return f"{name}: Train MSE ({round(train_result, 5)}) Test MSE({round(test_result, 5)})"

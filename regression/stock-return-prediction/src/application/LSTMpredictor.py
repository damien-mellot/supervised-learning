import numpy as np
import pandas as pd

from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_squared_error

from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import SGD
from keras.layers import LSTM
from keras.wrappers.scikit_learn import KerasRegressor

import src.settings.base as stg
from src.infrastructure.datasetcreation import DataFrameBuilder
from src.domain.datatransformation import DatasetTransformer


    
def create_LSTMmodel(learn_rate = 0.01, 
                     momentum = 0, 
                     validation_size = 0.2,
                     seq_len = 2):
    # Loading dataset and separate features from target
    dfb = DataFrameBuilder(stg.AMZN_data)
    X = dfb.features
    Y = dfb.target
    
    # Train - Test split
    train_size = int(len(X) * (1 - validation_size))
    X_train, X_test = X[0:train_size], X[train_size:len(X)]
    Y_train, Y_test = Y[0:train_size], Y[train_size:len(X)]
    
    # Train - Test split LSTM
    Y_train_LSTM, Y_test_LSTM = np.array(Y_train)[seq_len - 1:], np.array(Y_test)
    X_train_LSTM = np.zeros((X_train.shape[0] + 1 - seq_len, seq_len, X_train.shape[1]))
    X_test_LSTM = np.zeros((X_test.shape[0], seq_len, X.shape[1]))

    for i in range(seq_len):
        X_train_LSTM[:, i, :] = np.array(X_train)[i : X_train.shape[0] + i + 1 - seq_len, :]
        X_test_LSTM[:, i, :] = np.array(X)[X_train.shape[0] + i - 1 : X.shape[0] + i + 1 - seq_len, :]
    # Create model
    model = Sequential()
    model.add(LSTM(50, input_shape=(X_train_LSTM.shape[1], X_train_LSTM.shape[2])))
    
    # More cells can be added if needed
    model.add(Dense(units=1))
    optimizer = SGD(lr=learn_rate, momentum=momentum)
    model.compile(loss='mse', optimizer='adam')
        
    return model

def lstm_mse(validation_size = 0.2,
             seq_len = 2,
             name = 'Long short-term memory'):
    
    # Loading dataset and separate features from target
    dfb = DataFrameBuilder(stg.AMZN_data)
    X = dfb.features
    Y = dfb.target
    
    # Train - Test split
    train_size = int(len(X) * (1 - validation_size))
    X_train, X_test = X[0:train_size], X[train_size:len(X)]
    Y_train, Y_test = Y[0:train_size], Y[train_size:len(X)]
    
    # Train - Test split LSTM
    Y_train_LSTM, Y_test_LSTM = np.array(Y_train)[seq_len - 1:], np.array(Y_test)
    X_train_LSTM = np.zeros((X_train.shape[0] + 1 - seq_len, seq_len, X_train.shape[1]))
    X_test_LSTM = np.zeros((X_test.shape[0], seq_len, X.shape[1]))

    for i in range(seq_len):
        X_train_LSTM[:, i, :] = np.array(X_train)[i : X_train.shape[0] + i + 1 - seq_len, :]
        X_test_LSTM[:, i, :] = np.array(X)[X_train.shape[0] + i - 1 : X.shape[0] + i + 1 - seq_len, :]
    
    LSTM_model = create_LSTMmodel()
    LSTM_model_fit = LSTM_model.fit(X_train_LSTM, 
                                    Y_train_LSTM, 
                                    validation_data=(X_test_LSTM, Y_test_LSTM),
                                    epochs=330,
                                    batch_size=72,
                                    verbose=0,
                                    shuffle=False)
    
    error_Training_LSTM = mean_squared_error(Y_train_LSTM, LSTM_model.predict(X_train_LSTM))
    predicted = LSTM_model.predict(X_test_LSTM)
    error_Test_LSTM = mean_squared_error(Y_test, predicted)
    
    return f"{name}: Train MSE ({round(error_Training_LSTM, 5)}) Test MSE({round(error_Test_LSTM, 5)})"
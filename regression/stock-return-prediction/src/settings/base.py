"""Basic settings of the project.
Contains all configurations for the projectself.
Should NOT contain any secrets.
>>> import settings
>>> settings.DATA_DIR
"""
import os
from os import path
import sys

# Paths
REPO_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../"))
DATA_DIR = os.path.join(REPO_DIR, "data")
RAW_DATA_DIR = path.join(DATA_DIR, "raw")
INTERIM_DATA_DIR = path.join(DATA_DIR, "interim")
PROCESSED_DATA_DIR = path.join(DATA_DIR, "processed")
PREDICTION_DATA_DIR = path.join(DATA_DIR, "prediction")
PREDICTIONS_FILENAME = path.abspath(
    path.join(PREDICTION_DATA_DIR, "stock_prediction.csv")
)

# Utils
start = '2010-01-01'
end = '2020-01-01'
yahoo = 'yahoo'
fred = 'fred'

# DataFrame columns
# Microsoft MSFT
MSFT = 'MSFT'
MSFT_1M = "MSFT_1M"
MSFT_3M = "MSFT_3M"
MSFT_6M = "MSFT_6M"
MSFT_1Y = "MSFT_1Y"
IBM = 'IBM'
GOOGL = 'GOOGL'
VIXCLS= "VIXCLS"
DJIA = "DJIA"
SP500 = "SP500"
DEXUSUK = "DEXUSUK"
DEXJPUS = "DEXJPUS"

MSFT_PRED = "MSFT_pred"
MSFT_lagged = 'MSFT_pred(t-1)'
IBM_lagged = "IBM(t-1)"
GOOGL_lagged = "GOOGL(t-1)"
MSFT_1M_lagged = "MSFT_1M(t-1)"
MSFT_3M_lagged = "MSFT_3M(t-1)"
MSFT_6M_lagged = 'MSFT_6M(t-1)'
MSFT_1Y_lagged = "MSFT_1Y(t-1)"
VIXCLS_lagged = "VIXCLS(t-1)"
DJIA_lagged = "DJIA(t-1)"
SP500_lagged = "SP500(t-1)"
DEXUSUK_lagged = "DEXUSUK(t-1)"
DEXJPUS_lagged = "DEXJPUS(t-1)"
COLS = [MSFT_lagged, IBM_lagged, IBM_lagged, 
        DEXJPUS_lagged, DEXUSUK_lagged, 
        SP500_lagged, DJIA_lagged, VIXCLS_lagged,
        MSFT_1M_lagged, MSFT_3M_lagged, MSFT_6M_lagged, MSFT_1Y_lagged]

# Data
MSFT_R5_data = 'MSFT_R5_data.csv'
MSFT_R21_data = 'MSFT_R21_data.csv'

**Yield curve prediction**

A yield curve is a line that plots yields (interest rates) of bonds having equal credit quality but differing maturity dates. This yield curve is used as a benchmark for other debt in the market, such as mortgage rates or banking lending rates. The most frequently reported yield curve compares the 3-months, 2-years, 5-years, 10-years, and 30-years U.S Treasury debt.

The yield curve is the centerpiece in a fixed income market. Fixed income markets are important sources of finance for governments, national and supranational institutions, banks, and private and public corporations. In addition, yield curves are very important to investors in pension funds and insurance companies.

The yield curve is a key representation of the state of the bond market. Investors watch the bond market closely as it is a strong predictor of future economic activity and levels of inflation, which affects prices of goods, financial assets, and real estate. The slope of the yield curve (normal, inverted or flat) is an important indicator of short-term rates and is followed closely by investors.

Hence, an accurate yield curve forecasting is of critical importance in financial applications. Several statistical techniques and tools commonly used in econometrics and finance have been applied to model the yield curve.

In the supervised regression framework used for this case study, three tenors (i.e. 1M, 5Y and 30Y) of the yield curve are the predicted variable. These tenors represent short term, medium term and long-term tenors of the yield curve.

The goal of this case study is to use supervised learning-based models to predict the yield curve. This case study is inspired by the paper “Artificial Neural Networks in Fixed Income Markets for Yield Curve Forecasting” by Nunes, Gerding, McGroarty and Niranj (2018).

In this case study, we will focus on:

- Simultaneous modelling (producing multiple outputs at the same time) of the interest rates
- Comparison of neural networks versus linear regression models
- Modelling a time series in a supervised regression-based framework
- Understanding the variable intuition and feature selection



from fastapi import FastAPI
from chaos.application.api import predict
from chaos.infrastructure.CloudConnector import GCPConnector
import chaos.settings as stg
import os


GOOGLE_APPLICATION_CREDENTIALS = os.getenv("GOOGLE_APPLICATION_CREDENTIALS")
if GOOGLE_APPLICATION_CREDENTIALS:
    connector = GCPConnector("chaos-6")
    connector.download_blobs(
        {
            "data.csv": os.path.join(stg.RAW_DATA_DIR, "data.csv"),
            "socio_eco.csv": os.path.join(stg.RAW_DATA_DIR, "socio_eco.csv"),
            "model_rfc.pkl": os.path.join(stg.PROCESSED_DATA_DIR, "model_rfc.pkl"),
            "pipe.pkl": os.path.join(stg.PROCESSED_DATA_DIR, "pipe.pkl"),
        }
    )
app = FastAPI()
app.include_router(predict.router)

# if __name__ == "__main__":
#    uvicorn.run(app, host=HOST, port=PORT)

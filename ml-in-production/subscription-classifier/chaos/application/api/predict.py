from fastapi import Body, File, UploadFile, HTTPException, APIRouter
from typing import Optional
import pandas as pd
from chaos.domain.customer import Customer
import chaos.settings as stg
import os
from pydantic import BaseModel
import json
import numpy as np
from io import BytesIO

router = APIRouter()

# Création des routes
# /predict/ POST CREATE add a predict
#

TMP_FILE = os.path.join(stg.DATA_DIR, "tmp.csv")


class TelemarketingItem(BaseModel):
    DATE: str
    AGE: int
    JOB_TYPE: str
    STATUS: str
    EDUCATION: str
    HAS_DEFAULT: str
    BALANCE: int
    HAS_HOUSING_LOAN: str
    HAS_PERSO_LOAN: str
    CONTACT: str
    DURATION_CONTACT: int
    NB_CONTACT: int
    NB_DAY_LAST_CONTACT: Optional[int] = None
    NB_CONTACT_LAST_CAMPAIGN: int
    RESULT_LAST_CAMPAIGN: Optional[str] = ""


def _create_dataframe_from_predictions(df, predictions):
    df_pred = pd.DataFrame(predictions, columns=["Pred_0", "Pred_1"])
    return pd.concat([df, df_pred["Pred_1"]], axis=1)


def _get_predictions(file_to_predict):
    predicter = Customer(file_to_predict, stg.CSV_ECOPRED_PATH)
    predictions = predicter.predict_subscription_from_file()
    df_pred = pd.DataFrame(predictions, columns=["Pred_0", "Pred_1"])
    return df_pred


@router.post("/predict")
async def get_predict_from_json(
    item: TelemarketingItem = Body(
        ...,
        example={
            "DATE": "2020-03-31",
            "AGE": 50,
            "JOB_TYPE": "Technicien",
            "STATUS": "Marié",
            "EDUCATION": "Secondaire",
            "HAS_DEFAULT": "No",
            "BALANCE": 234,
            "HAS_HOUSING_LOAN": "No",
            "HAS_PERSO_LOAN": "No",
            "CONTACT": "Portable",
            "DURATION_CONTACT": 299,
            "NB_CONTACT": 1,
            "NB_DAY_LAST_CONTACT": -1,
            "NB_CONTACT_LAST_CAMPAIGN": 0,
            "RESULT_LAST_CAMPAIGN": "",
        },
    ),
):
    """Api endpoint to test only one line of marketing data

    Parameters
    ----------
    item : TelemarketingItem, optional
        Structure of Dataframe, by default Body( ..., example={ "DATE": "2020-03-31", "AGE": 50, "JOB_TYPE": "Technicien", "STATUS": "Marié", "EDUCATION": "Secondaire", "HAS_DEFAULT": "No", "BALANCE": 234, "HAS_HOUSING_LOAN": "No", "HAS_PERSO_LOAN": "No", "CONTACT": "Portable", "DURATION_CONTACT": 299, "NB_CONTACT": 1, "NB_DAY_LAST_CONTACT": -1, "NB_CONTACT_LAST_CAMPAIGN": 0, "RESULT_LAST_CAMPAIGN": "", }, )

    Returns
    -------
    Dict
        Dict with prediction
    """
    df_to_predict = pd.DataFrame([item.dict()])
    df_to_predict.to_csv(TMP_FILE, index=False, sep=stg.SEP)
    predictions = _get_predictions(TMP_FILE)
    return {"prediction": predictions["Pred_1"].to_dict()}


@router.post("/multiples_predict")
async def get_predict_from_csv(file: bytes = File(...)):
    """Api endpoints to test a batch of csv file

    Parameters
    ----------
    file : bytes, optional
        uploaded file with marketing data to predict, by default File(...)

    Returns
    -------
    Dict
        Dict with all predictions

    Raises
    ------
    HTTPException
        If File is empty
    """
    if not file:
        raise HTTPException(status_code=400, detail="File is empty")
    data = BytesIO(file)
    df_tampon = pd.read_csv(data, sep=stg.SEP)
    df_tampon.to_csv(TMP_FILE, index=False, sep=stg.SEP)
    df_to_predict = pd.read_csv(TMP_FILE, sep=stg.SEP)
    predictions = _get_predictions(TMP_FILE)
    df = pd.concat([df_to_predict, predictions["Pred_1"]], axis=1)
    predictions = predictions.fillna("")
    return {"prediction": predictions["Pred_1"].to_dict()}

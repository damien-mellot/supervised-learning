__all__ = ["config"]

import os
import yaml
import chaos.settings as stg

ENVIRONMENT = os.getenv("YOTTA_ML3_CONFIGURATION_PATH")
this_dir = os.path.dirname(os.path.realpath(__file__))

if ENVIRONMENT:
    config_file_path = ENVIRONMENT
else:
    config_file_path = os.path.join(this_dir, "config.yml")
with open(config_file_path, "r") as file_in:
    config = yaml.safe_load(file_in)

ENVIRONMENT = os.getenv("GOOGLE_APPLICATION_CREDENTIALS")
print(ENVIRONMENT)
if not ENVIRONMENT:
    if not os.path.isfile(
        os.path.join(stg.CONFIG_DIR, "yotta-square-ml3-25cdb1f9af42.json")
    ):
        print("Merci de nous contacter pour avoir accès au Storage GCP")
    else:
        os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = os.path.join(
            stg.CONFIG_DIR, "yotta-square-ml3-25cdb1f9af42.json"
        )

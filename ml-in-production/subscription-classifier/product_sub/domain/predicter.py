from product_sub.infrastructure.dataset_builder import DatasetBuilder
from product_sub.domain.pipeline_creator import PipelineCreator
import pandas as pd
import product_sub.settings as stg
from sklearn.ensemble import RandomForestClassifier
from imblearn.combine import SMOTEENN
import pickle
import os
from os.path import isfile


class Predicter:
    """
    Class to predict product subscription
    """

    def __init__(
        self, marketing_file, eco_file, preprocessor_pickle_path, model_pickle_path
    ):
        self.marketing_file = marketing_file
        self.eco_file = eco_file
        self.preprocessor_pickle_path = preprocessor_pickle_path
        self.model_pickle_path = model_pickle_path

    def _train(self):
        dataset_merged = DatasetBuilder(
            filename_bank=self.marketing_file, filename_socio=self.eco_file
        ).create_dataset()
        X_train = dataset_merged.drop(columns=stg.COL_RAW_SUBSCRIPTION)
        y_train = dataset_merged[stg.COL_RAW_SUBSCRIPTION].values
        preprocessor_pipeline = PipelineCreator().preprocessor
        X_train_processed = preprocessor_pipeline.fit_transform(X_train)
        smote_enn = SMOTEENN(
            sampling_strategy=0.8, random_state=stg.RANDOM_STATE, n_jobs=-1
        )
        X_train, y_train = smote_enn.fit_resample(X_train_processed, y_train)
        random_forest_classifier = RandomForestClassifier(**stg.RFC_PARAMS)
        random_forest_classifier.fit(X_train, y_train)
        self._dump_model(preprocessor_pipeline, random_forest_classifier)
        return preprocessor_pipeline, random_forest_classifier

    def _dump_model(self, preprocessor_pipeline, model):
        with open(self.preprocessor_pickle_path, "wb") as handle:
            pickle.dump(preprocessor_pipeline, handle, protocol=pickle.HIGHEST_PROTOCOL)
        with open(self.model_pickle_path, "wb") as handle:
            pickle.dump(model, handle, protocol=pickle.HIGHEST_PROTOCOL)

    def predict(self, bank_file, eco_file):
        """Get predictions from marketing and economical datas

        Parameters
        ----------
        bank_file : csv file
            csv file with telemarketing data
        eco_file : csv file
            csv file with economical data

        Returns
        -------
        np.Array
            Predict Proba
        """
        X_test = DatasetBuilder(
            filename_bank=bank_file,
            filename_socio=eco_file,
            is_test=True,
        ).create_dataset()
        if isfile(self.preprocessor_pickle_path) and isfile(self.model_pickle_path):
            preprocessor_pipeline, model = self._read_pickle()
        else:
            preprocessor_pipeline, model = self._train()
        X_test_transformed = preprocessor_pipeline.transform(X_test)
        predictions = model.predict_proba(X_test_transformed)
        return predictions

    def _read_pickle(self):
        with open(self.preprocessor_pickle_path, "rb") as handle:
            preprocessor = pickle.load(handle)
        with open(self.model_pickle_path, "rb") as handle:
            model = pickle.load(handle)
        return preprocessor, model


if __name__ == "__main__":

    pipe_file = os.path.join(stg.PROCESSED_DATA_DIR, "pipe.pkl")
    model_file = os.path.join(stg.PROCESSED_DATA_DIR, "model_rfc.pkl")
    test = Predicter(stg.FILENAME_BANK, stg.FILENAME_SOCIO_ECO, pipe_file, model_file)
    # test.dump_model(pipe_file=pipe_file, model_file=model_file)
    pred = test.predict(stg.FILENAME_DATA_TEST)
    print(pred)
    # preprocessor, model = test.read_pickle(pipe_file, model_file)
    # dbb = (
    # DatasetBuilder(stg.FILENAME_DATA_TEST, stg.FILENAME_SOCIO_ECO_TEST, is_test=True)
    # .create_dataset()
    # )
    # X_test_transformed = preprocessor.transform(dbb)
    # print(X_test_transformed)

.. Financial_Subscription documentation master file, created by
   sphinx-quickstart on Tue Jan 12 16:18:17 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Product_Subscription's documentation!
==================================================

.. warning::

   Work in progress!!!!!!

   *Release January 29th 2021*

   **Yotta Academy** 


| **Project Nº3**: *Machine Learning in production*

| **Subject Nº3**: *Chaos-6 / Product Subscription* 

| by **Emilio De Sousa** & **Damien Mellot**

===========================
Requirements
===========================

| Following tools must be install to setup this project:

   .. code-block:: bash

      $ python >= 3.8
      $ poetry >= 1.1

===========================
Setup environment
===========================

| Following command lines could be used to setup the project:

   .. code-block:: bash
      
      by SSH
      $ git clone git@gitlab.com:yotta-academy/mle-bootcamp/projects/ml-prod-projects/fall-2020/chaos-6.git
      by HTTPS
      https://gitlab.com/yotta-academy/mle-bootcamp/projects/ml-prod-projects/fall-2020/chaos-6.git
      $ cd chaos-6/
      $ poetry install  # Install virtual environment with packages from pyproject.toml file

===========================
Run script
===========================

| In order to run the script, following steps could be performed
| (only csv format is supported for now, contact us if you neeed):

   * Please put you data to predict and the file to train in the folder **data/raw**

   * For the train file, please name it *data.csv* or change the value of FILENAME_BANK in base.py

   * For the social context data file, please name it "socio_eco.csv" or change the value of **FILENAME_SOCIO_ECO** in base.py

   * After that run the following commands:

      .. code-block:: bash

         $ poetry shell 

         $ deposit_subscription --filename_to_predict  <filename_to_predict>

===========================
Find predictions
===========================

| After running the script, you will be able to find the prediction results in a **csv** file in the **data/processed** folder.

   * **0** is for **No**

   * **1** is for **Yes**

| *Care, you can overwrite it, please move it into another folder if you want to make another prediction*








      
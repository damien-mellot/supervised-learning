.. Financial_Subscription documentation master file, created by
sphinx-quickstart on Tue Jan 12 16:18:17 2021.
You can adapt this file completely to your liking, but it should at least
contain the root `toctree` directive.

# Welcome to Product_Subscription's documentation!


| **Project Nº3**: _Machine Learning in production_

| **Subject Nº3**: _Chaos-6 / Product Subscription_

| by **Emilio De Sousa** & **Damien Mellot**

===========================
Requirements
===========================

| Following tools must be install to setup this project:

.. code-block:: bash

      $ python >= 3.8
      $ poetry >= 1.1
      $ Docker Desktop >= 3.0

===========================
Setup environment
===========================

| Following command lines could be used to setup the project:

.. code-block:: bash

        - by SSH
        $ git clone git@gitlab.com:yotta-academy/mle-bootcamp/projects/ml-prod-projects/fall-2020/chaos-6.git
        - by HTTPS
        https://gitlab.com/yotta-academy/mle-bootcamp/projects/ml-prod-projects/fall-2020/chaos-6.git

        $ cd chaos-6/
        $ docker-compose build  # Build API and Streamlit images
        $ docker-compuse up -d # Run images

===========================
Access to Streamlit App
===========================

About the needed csv file, you can use **mkpred.csv** located in **data/**

# In order to access to the streamlit app, go to [localhost:8501](http://localhost:8501)

# Test the API

Thanks to Swagger, you can easily test the API by creating requests directly on the documentation page.
In order to test the API, you can go to [localhost:8000](http://localhost:8000/docs)
